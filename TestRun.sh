#!/bin/bash
BASE=~/sample
INPUT="$BASE/input"
OUTPUT="$BASE/output"
ARCHIVE="$BASE/archive"
LOG="$BASE/history.log"
FILES=$(ls "$INPUT")

mkdir -p "$INPUT"
mkdir -p "$OUTPUT"
mkdir -p "$ARCHIVE"
touch "$LOG"

while [ "$FILES" ]; do
   FILE=$(echo "$FILES" | head -1)
   grep "$FILES" "$LOG"
   if [ $? == 0 ]; then
       echo "already transcoding"
       sleep 10
   else
       echo "$FILE" >> "$LOG"
       mv "$INPUT/$FILE" "$ARCHIVE"

       NAME=${FILE%.[a-z]*}
       DIR_NAME="$OUTPUT/$NAME"

       mkdir -p "$DIR_NAME"

       # FORMAT REGULAR
       mkdir -p "$DIR_NAME/orig"
       cp "$ARCHIVE/$FILE" "$DIR_NAME/orig/"

       # FORMAT avi
       mkdir -p "$DIR_NAME/avi"
       ffmpeg -i "$ARCHIVE/$FILE" "$DIR_NAME/avi/$NAME.avi"
   fi
   FILES=$(ls "$INPUT")
done